clc
clear
close all

dist1 = 0.2325;
r1 = 0.015;
r2 = r1 - r1/2;
Piston.area1 = pi*r1*r1;
Piston.area2 = pi*r2*r2;
