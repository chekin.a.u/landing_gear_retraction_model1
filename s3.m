clc
clear

A = 5e-4;
P = 10e2;
F = P*A;

r = sqrt(A/pi);

r_orif = 0.001*0.00001;
A_orif = pi*r*r;

Spring.stiffness = 2*0;
Spring.damper = 1*0;

V = 10*A;
ro = 850;
m = V*ro
